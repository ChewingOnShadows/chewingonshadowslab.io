asset:
	rsync -a assets/ public/

gotenberg: 
	docker pull thecodingmachine/gotenberg:6
	docker run --rm -p 3000:3000 thecodingmachine/gotenberg:6 &
	sleep 3


# all resources need to at same level as index file
toPdf:
	curl --request POST \
		--url http://localhost:3000/convert/html \
		--header 'Content-Type: multipart/form-data' \
		--form files=@public/cv/index.html \
		--form files=@public/cv/cv-print.css \
		--form marginTop=0.25 \
		--form marginBottom=0.25 \
		--form marginLeft=0.25 \
		--form marginRight=0.25 \
		-o public/cv/Sten\ Holmberg\ CV.pdf

generate: gotenberg toPdf
	# stop gotenberg image
	docker ps | grep thecodingmachine/gotenberg | awk '{print $$1}' | xargs docker stop
