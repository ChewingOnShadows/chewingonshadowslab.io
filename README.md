# Unified CV

Inspired by this [markdown-cv project](https://github.com/elipapa/markdown-cv).

## NOTES

Gotenberg has some limitations when it comes to making pdf from html. Every file must be on the same diretory level as the index.html file. Also all images must be inlined as base64 strings. That's why the print css uses the icons in a weird manner. Still the best tool I found for conversion, almost works out of the box. wkhtmltopdf had issues with css columns in print format.

## TODO

- [ ] "hot load" templates
- [ ] live reload
- [ ] come up with some kind of api for templates
- [x] Add pdf generation to pipe.
      Seems like there's an issue with columns and wkhtmltopdf. Perhaps would need to try something like the Gotenberg docker image for conversions.
- [x] Make style for print (pdf)
- [x] Add favicon
- [x] Add pipe
  - [x] Build html and host on gitlab pages
