import report from "vfile-reporter";
import unified from "unified";
import remark from "remark-parse";
import remark2rehype from "remark-rehype";
import frontmatter from "remark-frontmatter";
import extract from "remark-extract-frontmatter";
import unwrapImages from "remark-unwrap-images";
import stringify from "rehype-stringify";
import { parse } from "yaml";
import { template } from "rehype-template";
import minify from "rehype-preset-minify";
import slug from "remark-slug";
import styleGuide from "remark-preset-lint-recommended";
import lintListIndent from "remark-lint-list-item-indent";
import vfileGlob from "vfile-glob";
import write from "vfile-write";
import { cv, post } from "./templates";
import { VFile } from "vfile";
import highlight from "rehype-highlight";
import { Template } from "./templates/types";

const templates = {
  cv,
  post,
};
export type TemplateName = keyof typeof templates;

// TODO figure out how to use the template specified from the frontmatter
// might have to make a unified plugin to wrap the selection?

const _build = (inputFile: VFile, targetTemplate: Template<any>) =>
  unified()
    .use(remark)
    .use(styleGuide)
    .use(unwrapImages)
    .use(lintListIndent, "mixed")
    .use(slug)
    .use(frontmatter)
    .use(extract, { yaml: parse })
    .use(remark2rehype)
    .use(minify)
    .use(highlight)
    .use(template, { template: targetTemplate })
    .use(stringify)
    .process(inputFile, (err, file) => {
      console.error(report(err || file));
      file.path = file.path.replace("src/sites/", "public/");
      file.extname = ".html";
      write(file); // this write function will create directories if not exists
    });

const build = (glob: string, template: TemplateName) => {
  const files = vfileGlob(glob);
  files.subscribe((file: VFile) => {
    const selectedTemplate = templates[template];
    _build(file, selectedTemplate);
  });
};

export default build;
