import { PostFrontmatter, Template } from "./types";

const { html, doctype } = require("rehype-template");

// the HAST is being really prickly with accessing undefined properties
// of the frontmatter.
function useFrontmatter(func: (frontmatter: PostFrontmatter) => any) {
  return (frontmatter: PostFrontmatter) => {
    try {
      return func(frontmatter);
    } catch {
      return null;
    }
  };
}
const renderAuthor = useFrontmatter(({ author }) => {
  if (!author) return null;
  return html`<small>by ${author}</small>`;
});

const renderPublishDate = useFrontmatter((frontmatter) => {
  if (!frontmatter.publishDate) return null;
  return html`<small
    >Published at <time>${frontmatter.publishDate}</time></small
  >`;
});

const renderHeroImage = useFrontmatter((frontmatter) => {
  return html`<img src="${frontmatter.heroImage}" id="hero-image" />`;
});
const renderOpenGraphData = useFrontmatter((frontmatter) => {
  return html`
    <meta property="og:title" content="${frontmatter.title}" />
    <meta property="og:description" content="A digital trash can." />
    <meta
      property="og:image"
      content="http://www.stenh.com/posts/${frontmatter.heroImage}"
    />
    <meta property="og:url" content="http://www.stenh.com" />
    <meta property="og:type" content="website" />
  `;
});

const t: Template<PostFrontmatter> = (node, frontmatter) => html`
  ${doctype}
  <html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>${frontmatter.title}</title>
      <link
        rel="stylesheet"
        type="text/css"
        href="/templates/post/post.css"
        media="screen"
      />
      ${renderOpenGraphData(frontmatter)}
    </head>
    <body>
      <article id="main">
        <header>
          <h1>${frontmatter.title}</h1>
          ${renderAuthor(frontmatter)}
        </header>
        ${renderHeroImage(frontmatter)} ${node}
        <footer>
          <nav>
            <a href="#">Previous post</a>
            <a href="#">Next post</a>
          </nav>
        </footer>
      </article>
      <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.4.1/highlight.min.js"></script>
      <script>
        hljs.initHighlightingOnLoad();
      </script>
    </body>
  </html>
`;
export default t;
