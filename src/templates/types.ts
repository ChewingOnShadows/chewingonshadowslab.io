export type Template<T> = (node: Node, frontmatter: T) => Node;
export type PostFrontmatter = {
  title: string;
  author?: string;
  publishDate?: string;
  slug?: string;
  heroImage?: string;
};
