import { Template } from "../types";
import { html, doctype } from "rehype-template";

const t: Template<any> = (node, frontmatter) => html`
  ${doctype}
  <html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>${frontmatter.title}</title>
      <link
        rel="stylesheet"
        type="text/css"
        href="/cv/cv-screen.css"
        media="screen"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="/cv/cv-print.css"
        media="print"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="favicon-32x32.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="favicon-16x16.png"
      />
    </head>
    <body>
      <div id="main">
        <div id="content">${node}</div>
        <div id="footer">
          <small id="icon-attribution"
            >Icons made by <a href="#">Freepik</a> from
            <a href="#">www.flaticon.com</a>
          </small>
        </div>
      </div>
    </body>
  </html>
`;

export default t;
