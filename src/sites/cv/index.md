---
layout: cv
title: Sten Holmberg CV
---

# Sten Holmberg

Full stack developer
[mail@stenh.com](mailto:mail@stenh.com)

## About me

Currently a frontend developer at [Klarna](http://www.klarna.com) through [DeviseSthlm](http://devisesthlm.se) consultancy firm. Working mostly with React and Typescript/Javascript to create components relating to card payments.

In my free time I enjoy climbing and game development.

## Work Experience

#### Klarna AB, Stockholm (contractor)

`May 2019 - now`

Front-end development focusing mostly on card-related purchases. My main task was creating and maintaining React components relating to card purchases that could be reused by other teams at Klarna for their payment methods. I also made frontends for new payment methods.

Examples of other tasks that I'm happy with:

- Set up more robust automated e2e testing
- Created a playground environment for card payments that's used for local development of React card purchase components, and also hosted it internally for other teams to use to get more familiar with card payments.

#### DeviseSthlm, Stockholm

`May 2019 - now`

Full time employee as a web developer consultant for DeviseSthlm.

#### Modular Finance, Stockholm

`May 2018 - 2019`

Full-stack developer with a heavy frontend focus. Most of my work was focused on developing a new web app that helps companies compose and publish press releases to the relevant channels. I was also responsible for the design of the UI and UX of the application.

I worked mostly with Angular and Javascript/Typescript for the frontend and Golang for the backend. The projets had a heavy focus on using micro-services for backend services that were managed with Kubernetes.

#### Datorn I Utbildningen, Stockholm

`2015-2018`

Part time job while I was studying at Stockholm University. My primary responsibilities were:

- Maintaining codebase for their custom conference booking system written in php.
- Developing some smaller web applications for different events and competitions relating to awarding and celebrating teachers who use computers in their education in novel ways.
- Handling operations of databases and blog sites.

## Education

#### Computer science, Stockholms Universitet

`2013 - 2017`

Three year bachelor for computer science at Stockholm University.

#### Digital Graphics, Nackademin

`2006 - 2008`

Two year vocational education for digital graphics. I went on to work as a 3D-artist for commercials and music videos for some years.

## Skills

#### Languages

- Swedish
- English

#### Frontend Development

- React
- Angular
- Javascript
- Typescript
- Webpack
- GraphQL

#### Backend Development

- NodeJS
- Docker
- SQL

#### Other

- Continuous Integration
- Unix
- Agile
