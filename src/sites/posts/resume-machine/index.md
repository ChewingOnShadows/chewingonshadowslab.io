---
title: A well-oiled resume machine
author: Sten Holmberg
publishDate: 2020-12-14
tags: javascript, markdown
heroImage: brrr.png
---

## The goal

As a challenge to myself I wanted to try to make my resume from markdown. My goals were to have the markdown file itself still look sensible and readable as is while providing a nicely rendered HTML page and pdf. I had been wanting to give [unifiedjs](https://unifiedjs.com/) a try for a while so this was a good opportunity I was thinking. Writing the markdown without any markup would also most likely force me to brush up on my css basics since there's going to be a lot of ugly selecting.

## The end result

Here are the different formats of the document. Markdown, desktop, mobile and pdf from left to right.

![document comparison](formats.png)

I think it looks decent enough!

## Building the HTML

So I set up a project with [gulp](https://gulpjs.com) and [unifiedjs](https://unifiedjs.com). Gulp will do the following:

- Build markdown using unifiedjs
- Build SASS files to CSS
- Compress images & copy other assets

What's the deal with unifiedjs? Well I thought it was interesting because it is a tool that can create abstract syntax trees from markdown (remark), html (rehype) and text (retext) and each AST has some unique plugins. For example with [retext](https://github.com/retextjs/retext) being a natural language processor you can use some pretty funky plugins, have a look at a [sample list of plugins](https://github.com/retextjs/retext/blob/main/doc/plugins.md) to get an idea. And the same is true for rehype and remark. With remark you can do a lot of linting and formatting of the markdown as adding table of contents and the like. And we can freely transform one AST to another in unified so we can utilize the whole ecosystem as long as we use markdown, html or natural language. Of course it also means we can output to any of these three formats, all formats have a stringify function which serializes the AST.

```typescript
// Example setup
unified()
  .use(remark) // parse to markdown AST
  .use(styleGuide) // add a markdown styleguide (lint)
  .use(slug) // create id slugs for headings `<h2 id="hello">Hello</h2>`
  .use(frontmatter) // parse frontmatter in markdown
  .use(extract, { yaml: parse }) // parse frontmatter in markdown
  .use(remark2rehype) // convert to html AST
  .use(template, { template: targetTemplate }) // configure template to use (defined below)
  .use(stringify)
  .process(inputFile, (err, outFile) => {
    // ... determine write path and other stuff here
    write(outFile);
  });

// An example template that the `template` plugin will use to create
// a full HTML document. Note that it also gets the frontmatter
type Template = (node: Node, frontmatter: Record<string, any>) => Node;
const targetTemplate: Template = (node, frontmatter) => html`
  ${doctype}
  <html>
    <head>
      <title>${frontmatter.title}</title>
    </head>
    <body>
      <div id="main">
        <div id="content">${node}</div>
      </div>
    </body>
  </html>
`;
```

And that's the basic setup. I created a gitlab pipe that would deploy the resume on a gitlab page when merged to master, but I felt it wasn't enough! What if I'm just about to go into a job interview and I discover a typo in my pdf that I (naturally) had intended to show the recruiter from my phone? Well, obviously the pipe also needs to build a pdf version.

## Building the PDF

I tried some alternatives to see what worked out of the box. A couple were nodejs and puppeteer solutions but they didn't work very well, a lot of the styling seemed to break. Also something about puppeteer gives me the creeps even though I've never used it. I wanted to use [wkhtmltopdf](https://wkhtmltopdf.org/), it seemed very light weight and very very fast. The problem I had with it was that there seemed to be some known bug with the css `column` attribute, and I very much wanted to use columns in the pdf layout. Other than that I think that this is a great tool.

In the end I settled on [gotenberg](https://github.com/thecodingmachine/gotenberg). It's **a lot** clunkier to use and slower to run but the output is perfect. It is used as a docker container that is spun up and you make requests to different endpoint for different transformations.

```makefile
toPdf:
  curl --request POST \
    --url http://localhost:3000/convert/html \
    --header 'Content-Type: multipart/form-data' \
    --form files=@public/cv/index.html \
    --form files=@public/cv/cv-print.css \
    --form marginTop=0.25 \
    --form marginBottom=0.25 \
    --form marginLeft=0.25 \
    --form marginRight=0.25 \
    -o public/cv/Sten\ Holmberg\ CV.pdf
```

There are Javascript and Golang APIs but since I will only be making one kind of request it felt enough with just a curl. It did have some warts though:

- All assets used by the html file needs to be at the same directory level as the html file.
- There are some edge cases with image usage, though I don't rememer exactly what they are. In my case I was referencing images in my css, but it didn't work to just add another `--form files=myIcon.svg` line. What I ended up doing was base64 encoding my icons and using that in my image urls.
- I don't really see the point of why it's a service and not a dockerized cli command, I'm sure there is one though.

The final step was adding this as a job in the `.gitlab.ci.yml` file. In gitlab CI a job can specify `services` as a list of docker images that needs to be running when the job runs. My problem was that there was no way to express port mapping (like in docker-compose) for the services. I think what you're supposed to do is refer to `http://container_name:port` instead. I was a bit reluctant to do that because I would have to run a different command locally (where I'm not running a docker-compose environment and want to target localhost). In the end what I did was I aliased the container name in gitlab CI as localhost.

```yaml
pages:
  stage: deploy
  services:
    - name: thecodingmachine/gotenberg:6
      alias: localhost # override localhost with container handle
  script:
    - npm run build # build the HTML since the PDF is created from it
    - make toPdf # runs the curl command specified above
  artifacts:
    paths:
      - public
  only:
    - master
```

And done! Next step is for the pipe to trigger automatic business card printing with on-demand drone delivery.
