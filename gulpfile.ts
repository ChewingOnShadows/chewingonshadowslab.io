import { parallel, series, src, dest, watch } from "gulp";
import markdownToHtml from "./src/buildMarkdown";
import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
// @ts-ignore
import sass from "gulp-dart-sass";
// @ts-ignore
import clean from "gulp-clean";

type CB = () => void;

function cleanup(cb: CB) {
  src("public", { read: false }).pipe(clean());
  cb();
}

const templateGlob = "src/templates/**/*.{ts,scss}";
const postsGlob = "src/sites/posts/**/*.md";
const cvGlob = "src/sites/cv/index.md";
const allContentGlob = [postsGlob, cvGlob, templateGlob];
function contentPosts(cb: CB) {
  markdownToHtml(postsGlob, "post");
  cb();
}
function contentCV(cb: CB) {
  markdownToHtml(cvGlob, "cv");
  cb();
}
const content = parallel(contentCV, contentPosts);

const otherAssetGlob = "./src/sites/**/*.{html,ico}";
const imageAssetGlob = "./src/sites/**/*.+(jpg|jpeg|gif|png)";
function otherAssets(cb: CB) {
  src(otherAssetGlob).pipe(dest("public"));
  cb();
}

function imageAssets(cb: CB) {
  src(imageAssetGlob)
    .pipe(
      imagemin([
        // imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 3 }),
      ])
    )
    .pipe(dest("public"));
  cb();
}

const assets = parallel(imageAssets, otherAssets);

const sitesStylesGlob = "src/sites/**/*.scss";
const templateStylesGlob = "src/templates/**/*.scss";
function style(cb: CB) {
  src(sitesStylesGlob)
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(dest("public/"));
  src(templateStylesGlob)
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(dest("public/templates"));
  cb();
}

const build = series(content, assets, style);

exports.build = build;
exports.style = style;
exports.other = otherAssets;
exports.image = imageAssets;
exports.clean = cleanup;
exports.cv = contentCV;

exports.watch = () => {
  build(console.log);
  watch([sitesStylesGlob, templateStylesGlob], style);
  watch(allContentGlob, content);
  watch([otherAssetGlob, imageAssetGlob], assets);
};
